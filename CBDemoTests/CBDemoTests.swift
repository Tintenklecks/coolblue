//
//  CBDemoTests.swift
//  CBDemoTests
//
//  Created by Ingo on 16.07.18.
//  Copyright © 2018 Ingo. All rights reserved.
//

import XCTest
@testable import CBDemo

class CBDemoTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testProductList() {
        let expectation: XCTestExpectation? = self.expectation(description: "Got Product List")
        Backend.getProductList(onSucess: { (productList) in
            if productList.products.count > 0 {
                expectation?.fulfill()
            } else {
                XCTFail("No products available")
            }
        }) { (errorCode) in
            XCTFail("Error \(errorCode)")
            
        }
        
        
        waitForExpectations(timeout: 5, handler: { error in
            if error != nil {
                if let aDescription = expectation?.description {
                    XCTFail("Error (\(aDescription))")
                }
            }
        })
    }
    
    
    func testProductDetails() {
        var productsChecked = 0
        let expectation: XCTestExpectation? = self.expectation(description: "Got All Product Details")
        Backend.getProductList(onSucess: { (productList) in
            if productList.products.count > 0 {
                
                for product in productList.products {
                    Backend.getProductDetail(product: product.productID, onSucess: { (detail) in
                        productsChecked += 1
                        if productsChecked == productList.products.count {
                            expectation?.fulfill()
                        }
                    }, onError: { (errorCode) in
                        XCTFail("Detail Error \(errorCode) for product with ID \(product.productID)")
                    })
                }
                
            } else {
                XCTFail("No products available")
            }
        }) { (errorCode) in
            XCTFail("Error \(errorCode)")
            
        }
        
        
        waitForExpectations(timeout: 20, handler: { error in
            if error != nil {
                if let aDescription = expectation?.description {
                    XCTFail("Error (\(aDescription))")
                }
            }
        })
    }
    
    
    
}
