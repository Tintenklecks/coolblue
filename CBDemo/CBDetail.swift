// To parse the JSON, add this file to your project and do:
//

import Foundation

struct DTCBDetail: Codable {
    let product: DTProduct
}

struct DTProduct: Codable {
    let productID: Int
    let productName, productText: String
    let reviewInformation: DTReviewInformation
    let usPS, pros, cons: [String]
    let availabilityState, salesPriceIncVat: Int
    let salesPriceExVat: Double
    let productImages, deliveredWith: [String]
    let specificationSummary: [DTSpecificationSummary]
    let nextDayDelivery: Bool
    let recommendedAccessories: [Int]
    
    enum CodingKeys: String, CodingKey {
        case productID = "productId"
        case productName, productText, reviewInformation
        case usPS = "USPs"
        case pros, cons, availabilityState, salesPriceIncVat, salesPriceExVat, productImages, deliveredWith, specificationSummary, nextDayDelivery, recommendedAccessories
    }
}

struct DTReviewInformation: Codable {
    let reviews: [DTReview]
    let reviewSummary: DTReviewSummary
}

struct DTReviewSummary: Codable {
    let reviewAverage: Double
    let reviewCount: Int
}

struct DTReview: Codable {
    let reviewID: Int
    let pros, cons: [String]
    let creationDate, creatorName, description, title: String
    let rating: Double
    let languageCode: String
    
    enum CodingKeys: String, CodingKey {
        case reviewID = "reviewId"
        case pros, cons, creationDate, creatorName, description, title, rating, languageCode
    }
}

struct DTSpecificationSummary: Codable {
    let name: String
    let value: DTValue
    let stringValue: String?
    let booleanValue: Bool?
}

enum DTValue: Codable {
    case bool(Bool)
    case string(String)
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(Bool.self) {
            self = .bool(x)
            return
        }
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        throw DecodingError.typeMismatch(DTValue.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for DTValue"))
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .bool(let x):
            try container.encode(x)
        case .string(let x):
            try container.encode(x)
        }
    }
}
