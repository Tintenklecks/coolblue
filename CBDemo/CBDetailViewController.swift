//
//  CBDetailViewController.swift
//  CBDemo
//
//  Created by Ingo on 18.07.18.
//  Copyright © 2018 Ingo. All rights reserved.
//

import UIKit
import Kingfisher

class CBDetailViewController: UIViewController {
    
    @IBOutlet weak var articleContent: UIScrollView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var productTitle: UILabel!
    @IBOutlet weak var imagePageControl: UIPageControl!
    
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var highlightsLabel: UILabel!
    
    @IBOutlet var starImages: [UIImageView]!
    
    var isLoaded = false
    
    var productId: Int {
        set {
            Backend.getProductDetail( product : newValue, onSucess: { (product) in
                self.productDetail = product.product
                DispatchQueue.main.async{
                    self.reloadData()
                }
            }) { (error) in
                let alertView = UIAlertController(title: "Error \(error)", message: "Error while retrieving data", preferredStyle: .alert)
                alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alertView, animated: true, completion: nil)
            }
            
            
        }
        get {
            if let productDetail = productDetail {
                return productDetail.productID
            } else {
                return -1
            }
        }
    }
    var productDetail : DTProduct? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        priceView.layer.cornerRadius = priceView.bounds.size.width/2
        priceLabel.rotate(angle: 15)
        scrollView.contentSize = articleContent.bounds.size

        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        isLoaded = true
        reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func reloadData() {
        
        
        guard isLoaded, let product = self.productDetail else {
            return
        }
        
        
        self.imagePageControl.numberOfPages = product.productImages.count

        if imagePageControl.numberOfPages > 0 {
            self.productImage.kf.setImage(with: URL(string:  product.productImages[0]), placeholder: #imageLiteral(resourceName: "placeholder.png"))
        }
        self.productTitle.text = product.productName
        
        let strippedString = product.productText.replacingOccurrences(of: "<[^>]+>", with: "", options: String.CompareOptions.regularExpression, range: nil)

        self.descriptionLabel.text = strippedString
        
        let pros = product.pros.joined(separator: "\n")
        let cons = product.cons.joined(separator: "\n")
        self.highlightsLabel.text =  "Pros\n\(pros)\n\nCons\n\(cons)"

        self.priceLabel.text = "\(product.salesPriceIncVat),-"
        
        
        let ranking = product.reviewInformation.reviewSummary.reviewAverage / 2
        for starImage in starImages {
            let rankingImage = Double(starImage.tag)
            if ranking >= rankingImage {
                starImage.image = #imageLiteral(resourceName: "starFull.png")
            } else if ranking >= rankingImage - 0.5 {
                starImage.image = #imageLiteral(resourceName: "halfStarGray.png")
            } else {
                starImage.image = #imageLiteral(resourceName: "starGray.png")
            }
        }

    }
    
}


