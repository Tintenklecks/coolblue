//
//  CBTableViewController.swift
//  CBDemo
//
//  Created by Ingo on 17.07.18.
//  Copyright © 2018 Ingo. All rights reserved.
//

import UIKit
import Kingfisher

class CBTableViewController: UITableViewController, UISearchBarDelegate {
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private let refresh = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension;
        self.tableView.estimatedRowHeight = 320;
        
        // Add Refresh Control to Table View
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refresh
        } else {
            tableView.addSubview(refresh)
        }
        refresh.addTarget(self, action: #selector(loadData), for: .valueChanged)


        
        loadData()
    }
    
   @objc func loadData() {
        infoView.backgroundColor = view.backgroundColor
        infoView.isHidden = false
        
        self.infoLabel.text = "Loading product list"
        
        Backend.getProductList(onSucess: { (cbList) in
            DispatchQueue.main.async{
                self.activity.stopAnimating()
          //      self.infoView.isHidden = true
                

                if Backend.products(filtered: self.searchBar.text).count == 0 {
                    self.infoLabel.text = "No products available"
                }
                self.refresh.endRefreshing()
                self.tableView.reloadData()

            }
            
            
            
            
        }) { (error) in
            self.tableView.reloadData()
//
            DispatchQueue.main.async{
                self.activity.stopAnimating()
                self.activity.stopAnimating()
                self.infoLabel.text = "Error \(error) occurred while loading product list"
                self.refresh.endRefreshing()
                self.tableView.reloadData()


            }
            let alertView = UIAlertController(title: "Error \(error)", message: "Error while retrieving data", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alertView, animated: true, completion: nil)
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = Backend.products(filtered: searchBar.text).count
        infoView.isHidden = count != 0
        self.infoLabel.text = "No products available"

        return count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductCell", for: indexPath) as? CBProductCell,
            indexPath.row <  Backend.products(filtered: searchBar.text).count
            else {
                return UITableViewCell(style: .default, reuseIdentifier: nil)
        }
        
        
        let productInfo = Backend.products(filtered: searchBar.text)[indexPath.row]
        
        
        cell.productLabel.text = productInfo.productName
        cell.highlightsLabel.text =  productInfo.usPS.joined(separator: "\n")
        cell.priceLabel.text = productInfo.salesPriceIncVat.formattedValue(2)
        //     let img = Kingfisher.init(URL(string: productInfo.productImage))
        
        cell.productImageView.kf.setImage(with: URL(string: productInfo.productImage), placeholder:#imageLiteral(resourceName: "placeholder.png")    )
        cell.productImageView.kf.setImage(
            with: URL(string: productInfo.productImage),
            placeholder: #imageLiteral(resourceName: "placeholder.png"), options: nil, progressBlock: { (val1, val2) in
                print(val1," ... ",val2)
        }) { (image, error, _, _) in
            print ("******",error ?? "")
            if error == nil,
                let image = image {
                
                let resizeImage = image.scaleToFit(size: cell.productImageView.bounds.size)
                cell.productImageView.image = resizeImage
            }
        }
        
        let ranking = productInfo.reviewInformation.reviewSummary.reviewAverage / 2
        for starImage in cell.starImage {
            let rankingImage = Double(starImage.tag)
            if ranking >= rankingImage {
                starImage.image = #imageLiteral(resourceName: "starFull.png")
            } else if ranking >= rankingImage - 0.5 {
                starImage.image = #imageLiteral(resourceName: "halfStarGray.png")
            } else {
                starImage.image = #imageLiteral(resourceName: "starGray.png")
            }
        }
        
        
        return cell
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CBDetailViewController,
            let indexPath = tableView.indexPathForSelectedRow,
            indexPath.row < Backend.products(filtered: searchBar.text).count {
            
            vc.productId = Backend.productList[indexPath.row].productID
        }
    }

    
    
    
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) { // called when text changes (including clear)
        print(searchText)
        tableView.reloadData()
    }
    
}

class CBProductCell: UITableViewCell {
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var highlightsLabel: UILabel!
    @IBOutlet var starImage: [UIImageView]!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceCircle: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //   self.backgroundColor = CI.lightBackgroundColor
        priceCircle.layer.cornerRadius = priceCircle.bounds.size.width / 2
        priceLabel.rotate(angle: 15)
    }
    
}



