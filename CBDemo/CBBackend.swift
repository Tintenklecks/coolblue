//
//  CBBackend.swift
//  CBDemo
//
//  Created by Ingo on 17.07.18.
//  Copyright © 2018 Ingo. All rights reserved.
//

import Foundation

class Backend {
    
    static var productList: [Product] = []
    
    static func products(filtered by: String?) -> [Product] {
        guard let filterString = by, filterString != "" else {
            return Backend.productList
        }
        
        return Backend.productList.filter( { (product: Product) -> Bool in
            let string = product.productName + product.usPS.joined()
            return string.lowercased().range(of:filterString.lowercased()) != nil
            
        })

        
    }
    
    
    static func getProductList(onSucess: @escaping (_ productList: CBList) -> Void,
                        onError: @escaping (_ errorCode: Int) -> Void = { errorNo in print(errorNo) } )  {
        
        Backend.productList = []

        let urlString = "https://bdk0sta2n0.execute-api.eu-west-1.amazonaws.com/ios-assignment/search?query=apple&page=1"
        guard let url = URL(string:  urlString) else {
            // ERROR URL NOT OK
            onError(-1)
            return
        }
        var request = URLRequest(url: url)
        request.timeoutInterval = 5

        request.httpMethod = "GET"
        request.cachePolicy = .reloadIgnoringLocalCacheData
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        let task = URLSession.shared.dataTask(with: request) {
            data,response,error in
            
            guard let response = response as? HTTPURLResponse else {
                // ERROR CASE NO RESPONSE
                onError(-2)
                return
            }
            let responseCode = response.statusCode
            
            guard responseCode == 200  else {
                // ERROR CASE BAD RESPONSE
                onError(-3)
                return
            }
            
            
            guard let data = data,
                let returnString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else {
                    // ERROR CASE BAD DATA
                    onError(-4)
                    return
            }
            
            guard let jsonData = returnString.data(using: String.Encoding.utf8.rawValue) else {
                // ERROR CASE BAD JSON
                onError(-5)
                return
            }
            
            do {
                let cbList = try JSONDecoder().decode(CBList.self, from: jsonData)
                Backend.productList = cbList.products

                onSucess(cbList)
                
            } catch {
                onError(-6)
            }
            
            
        }
        task.resume()
        
        
    }


    static func getProductDetail(product id: Int, onSucess: @escaping (_ productList: DTCBDetail) -> Void,
                        onError: @escaping (_ errorCode: Int) -> Void = { errorNo in print(errorNo) } )  {
        let urlString = "https://bdk0sta2n0.execute-api.eu-west-1.amazonaws.com/ios-assignment/product/\(id)"
        guard let url = URL(string:  urlString) else {
            // ERROR URL NOT OK
            onError(-1)
            return
        }
        var request = URLRequest(url: url)
        request.timeoutInterval = 5
        request.httpMethod = "GET"
        request.cachePolicy = .reloadIgnoringLocalCacheData
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        
        
        let task = URLSession.shared.dataTask(with: request) {
            data,response,error in
            
            guard let response = response as? HTTPURLResponse else {
                // ERROR CASE NO RESPONSE
                onError(-2)
                return
            }
            let responseCode = response.statusCode
            
            guard responseCode == 200  else {
                // ERROR CASE BAD RESPONSE
                onError(-3)
                return
            }
            
            
            guard let data = data,
                let returnString = NSString(data: data, encoding: String.Encoding.utf8.rawValue) else {
                    // ERROR CASE BAD DATA
                    onError(-4)
                    return
            }
            
            guard let jsonData = returnString.data(using: String.Encoding.utf8.rawValue) else {
                // ERROR CASE BAD JSON
                onError(-5)
                return
            }
            
            do {
                let cbDetail = try JSONDecoder().decode(DTCBDetail.self, from: jsonData)
                onSucess(cbDetail)
                
            } catch {
                onError(-6)
            }
            
            
        }
        task.resume()
        
        
    }
}
